# translation of plasma_runner_webshortcuts.po to Khmer
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2008, 2009, 2010.
# Auk Piseth <piseth_dv@khmeros.info>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_webshortcuts\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-02 00:47+0000\n"
"PO-Revision-Date: 2010-03-15 16:03+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: webshortcutrunner.cpp:58
#, kde-format
msgid "Opens \"%1\" in a web browser with the query :q:."
msgstr "បើក \"%1\" នៅ​ក្នុង​កម្មវិធី​រុករក​បណ្ដាញ​ដែល​មាន​សំណួរ :q:."

#: webshortcutrunner.cpp:63
#, kde-format
msgid "Search using the DuckDuckGo bang syntax"
msgstr ""

#: webshortcutrunner.cpp:94
#, kde-format
msgid "Search in private window"
msgstr ""

#: webshortcutrunner.cpp:94
#, kde-format
msgid "Search in incognito window"
msgstr ""

#: webshortcutrunner.cpp:129 webshortcutrunner.cpp:146
#, kde-format
msgid "Search %1 for %2"
msgstr "ស្វែងរក %1 សម្រាប់ %2"
