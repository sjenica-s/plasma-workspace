# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Volkan Gezer <volkangezer@gmail.com>, 2015, 2017, 2021.
# Emir SARI <emir_sari@icloud.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-09 00:47+0000\n"
"PO-Revision-Date: 2022-08-09 12:28+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Volkan Gezer, Emir SARI"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "volkangezer@gmail.com, emir_sari@icloud.com"

#: kcm.cpp:590
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr ""
"Bu değişikliklerin etkili olması için Plasma oturumunu yeniden başlatmanız "
"gerekir."

#: kcm.cpp:591
#, kde-format
msgid "Cursor Settings Changed"
msgstr "İmleç Ayarları Değiştirildi"

#: lnftool.cpp:33
#, kde-format
msgid "Global Theme Tool"
msgstr "Genel Tema Aracı"

#: lnftool.cpp:35
#, kde-format
msgid ""
"Command line tool to apply global theme packages for changing the look and "
"feel."
msgstr ""
"Görünümü ve hissi değiştirmek ile genel tema paketlerini uygulamak için "
"komut satırı aracı."

#: lnftool.cpp:37
#, kde-format
msgid "Copyright 2017, Marco Martin"
msgstr "Telif Hakkı 2017, Marco Martin"

#: lnftool.cpp:38
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: lnftool.cpp:38
#, kde-format
msgid "Maintainer"
msgstr "Bakımcı"

#: lnftool.cpp:46
#, kde-format
msgid "List available global theme packages"
msgstr "Mevcut genel tema paketlerini listele"

#: lnftool.cpp:49
#, kde-format
msgid ""
"Apply a global theme package. This can be the name of a package, or a full "
"path to an installed package, at which point this tool will ensure it is a "
"global theme package and then attempt to apply it"
msgstr ""
"Genel bir tema paketi uygula. Bu, bir paketin adı veya kurulu bir paketin "
"tam yolu olabilir; bu noktada bu araç, bunun genel bir tema paketi "
"olduğundan emin olur ve ardından uygulamaya çalışır."

#: lnftool.cpp:51
#, kde-format
msgid "packagename"
msgstr "paketadı"

#: lnftool.cpp:52
#, kde-format
msgid "Reset the Plasma Desktop layout"
msgstr "Plasma Masaüstü düzenini sıfırla"

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: lookandfeelsettings.kcfg:9
#, kde-format
msgid "Global look and feel"
msgstr "Genel görünüm ve his"

#: package/contents/ui/main.qml:20
#, kde-format
msgid "This module lets you choose the global look and feel."
msgstr "Bu modül, genel görünümü ve hissi seçmenize olanak tanır."

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Contains Desktop layout"
msgstr "Masaüstü düzeni içerir"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Preview Theme"
msgstr "Temayı Önizle"

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt ""
"Confirmation question about applying the Global Theme - %1 is the Global "
"Theme's name"
msgid "Apply %1?"
msgstr "%1 uygulansın mı?"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Choose what to apply…"
msgstr "Neyin uygulanacağını seçin…"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Show fewer options…"
msgstr "Daha az seçenek göster…"

#: package/contents/ui/main.qml:118
#, kde-format
msgid "Apply"
msgstr "Uygula"

#: package/contents/ui/main.qml:130
#, kde-format
msgid "Cancel"
msgstr "İptal"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Get New Global Themes…"
msgstr "Yeni Genel Temalar Al…"

#: package/contents/ui/MoreOptions.qml:24
#: package/contents/ui/MoreOptions.qml:31
#, kde-format
msgid "Layout settings:"
msgstr "Düzen ayarları:"

#: package/contents/ui/MoreOptions.qml:32
#, kde-format
msgid "Desktop layout"
msgstr "Masaüstü düzeni"

#: package/contents/ui/MoreOptions.qml:53
#, kde-format
msgid "Titlebar Buttons layout"
msgstr "Başlık Çubuğu Düğmeleri Düzeni"

#: package/contents/ui/MoreOptions.qml:66
#: package/contents/ui/SimpleOptions.qml:63
#, kde-format
msgid ""
"Applying a Desktop layout replaces your current configuration of desktops, "
"panels, docks, and widgets"
msgstr ""
"Bir masaüstü düzeni uygulamak; geçerli masaüstü, panel, rıhtım ve araç "
"takımı yapılandırmanızı değiştirir"

#: package/contents/ui/MoreOptions.qml:75
#, kde-format
msgid "Appearance settings:"
msgstr "Görünüş ayarları:"

#: package/contents/ui/MoreOptions.qml:78
#, kde-format
msgid "Colors"
msgstr "Renkler"

#: package/contents/ui/MoreOptions.qml:79
#, kde-format
msgid "Application Style"
msgstr "Uygulama Biçemi"

#: package/contents/ui/MoreOptions.qml:80
#, kde-format
msgid "Window Decorations"
msgstr "Pencere Dekorasyonları"

#: package/contents/ui/MoreOptions.qml:81
#, kde-format
msgid "Icons"
msgstr "Simgeler"

#: package/contents/ui/MoreOptions.qml:82
#, kde-format
msgid "Plasma Style"
msgstr "Plasma Biçemi"

#: package/contents/ui/MoreOptions.qml:83
#, kde-format
msgid "Cursors"
msgstr "İmleçler"

#: package/contents/ui/MoreOptions.qml:84
#, kde-format
msgid "Fonts"
msgstr "Yazıtipleri"

#: package/contents/ui/MoreOptions.qml:85
#, kde-format
msgid "Task Switcher"
msgstr "Görev Değiştirici"

#: package/contents/ui/MoreOptions.qml:86
#, kde-format
msgid "Splash Screen"
msgstr "Açılış Ekranı"

#: package/contents/ui/MoreOptions.qml:87
#, kde-format
msgid "Lock Screen"
msgstr "Kilit Ekranı"

#: package/contents/ui/SimpleOptions.qml:19
#, kde-format
msgid "The following will be applied by this Global Theme:"
msgstr "Bu Global Tema ile aşağıdakiler uygulanacaktır:"

#: package/contents/ui/SimpleOptions.qml:32
#, kde-format
msgid "Appearance settings"
msgstr "Görünüş ayarları"

#: package/contents/ui/SimpleOptions.qml:42
#, kde-format
msgctxt "List item"
msgid "• Appearance settings"
msgstr "• Görünüş ayarları"

#: package/contents/ui/SimpleOptions.qml:47
#, kde-format
msgid "Desktop and window layout"
msgstr "Masaüstü ve pencere düzeni"

#: package/contents/ui/SimpleOptions.qml:58
#, kde-format
msgctxt "List item"
msgid "• Desktop and window layout"
msgstr "• Masaüstü ve pencere düzeni"

#: package/contents/ui/SimpleOptions.qml:75
#, kde-format
msgid ""
"This Global Theme does not provide any applicable settings. Please contact "
"the maintainer of this Global Theme as it might be broken."
msgstr ""
"Bu Global Tema, uygulanabilir bir ayar sunmuyor. Bozulmuş olabileceğinden, "
"lütfen geliştiricisini durumdan haberdar edin."
